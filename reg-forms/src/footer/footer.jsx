import React, { Component } from "react";
import "./footer.scss";

class Footer extends Component {
  render() {
    return (
      <div className="footerPage">
        <div className="display">
          <p>
            <small>Copyright © 2019 Registration Page</small>
          </p>
        </div>
      </div>
    );
  }
}
export default Footer;
