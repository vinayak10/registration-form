import React, { Component } from "react";
import Navbar from "./navpage/navbar";
import CoverPage from "./coverpage/coverpage";
import Footer from "./footer/footer";

class HomePage extends Component {
  render() {
    return (
      <div className="App">
        <Navbar />
        <CoverPage />
        <Footer />
      </div>
    );
  }
}

export default HomePage;
