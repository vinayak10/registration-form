import React, { Component } from "react";
import "./coverpage.scss";
import axios from "axios";

class CoverPage extends Component {
  state = {
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    cnfmPassword: "",
    mobile: "",
    gender: "male",
    member: "",
    fieldRequired: false,
    showValidation: [],
    error: ""
  };

  sendData = e => {
    const {
      lastName,
      firstName,
      email,
      password,
      mobile,
      gender,
      member
    } = this.state;
    if (
      lastName == "" ||
      firstName == "" ||
      email == "" ||
      password == "" ||
      mobile == "" ||
      gender == "" ||
      member == ""
    ) {
      return this.setState({ fieldRequired: true });
    } else this.setState({ fieldRequired: false });
    const data = this.state;

    axios
      .post("http://159.65.145.171:4000/api/form", data)
      .then(res => {
        if (res.data.error) {
          return this.setState({ error: res.data.error });
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  validateFirstName = e => {
    const { firstName } = this.state;
    if (!firstName) return "*First Name is required";
  };

  validateLastName = e => {
    const { lastName } = this.state;
    if (!lastName) return "*Last Name is required";
  };

  validateEmail = e => {
    const { email } = this.state;
    if (!email) return "*Email is required";
    else if (!email.includes("@")) return "*Invalid Email";
  };

  validatePassword = e => {
    const { password } = this.state;
    if (!password) return "*Password is required";
    else if (password.length < 8)
      return "*Password needs to be more than 8 characters long";
  };

  validateCnfmPassword = e => {
    const { cnfmPassword } = this.state;
    if (!cnfmPassword) return "*Confirm Password is required";
    else if (cnfmPassword !== this.state.password)
      return "*password doesn't match";
    else return "";
  };

  validateNumber = e => {
    const { mobile } = this.state;
    let phoneno = /^\d{10}$/;
    if (!mobile) return "*mobile number is required";
    else if (mobile !== phoneno && mobile.length != 10)
      return "*mobile no. must contain 10 digit";
  };

  showValidation = e => {
    let temp = [...this.state.showValidation];
    if (temp.indexOf(e.target.name) > -1) {
      return;
    } else {
      temp.push(e.target.name);
      this.setState({ showValidation: temp });
    }
  };

  render() {
    const {
      firstName,
      lastName,
      email,
      password,
      cnfmPassword,
      mobile,
      member
    } = this.state;

    return (
      <div className="coverPage">
        <div className="form-input">
          <form onSubmit={e => e.preventDefault()}>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-2 col-form-label">
                Name
              </label>

              <div className="col-sm-5">
                <input
                  value={firstName}
                  type="text"
                  name="firstName"
                  className="form-control"
                  placeholder="First name"
                  onBlur={this.showValidation}
                  onInput={this.validateFirstName}
                  onChange={e => this.setState({ firstName: e.target.value })}
                />
                <short style={{ color: "red" }}>
                  {this.state.showValidation.includes("firstName") &&
                    this.validateFirstName()}
                </short>
              </div>
              <div className="col-sm-5">
                <input
                  value={lastName}
                  type="text"
                  name="lastName"
                  className="form-control"
                  placeholder="Last name"
                  onBlur={this.showValidation}
                  onInput={this.validateLastName}
                  onChange={e => this.setState({ lastName: e.target.value })}
                />
                <short style={{ color: "red" }}>
                  {this.state.showValidation.includes("lastName") &&
                    this.validateLastName()}
                </short>
              </div>
            </div>
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-2 col-form-label">
                Email
              </label>
              <div class="col-sm-10">
                <input
                  value={email}
                  type="email"
                  class="form-control"
                  id="inputEmail3"
                  name="email"
                  placeholder="Email"
                  onBlur={this.showValidation}
                  onInput={this.validateEmail}
                  onChange={e => this.setState({ email: e.target.value })}
                />

                {!this.state.error ? (
                  <short style={{ color: "red" }}>
                    {this.state.showValidation.includes("email") &&
                      this.validateEmail()}
                  </short>
                ) : (
                  <short style={{ color: "red" }}>{this.state.error}</short>
                )}
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword3" class="col-sm-2 col-form-label">
                Password
              </label>
              <div class="col-sm-10">
                <input
                  value={password}
                  type="password"
                  class="form-control"
                  id="inputPassword3"
                  name="password"
                  placeholder="Password"
                  onBlur={this.showValidation}
                  onInput={this.validatePassword}
                  onChange={e => this.setState({ password: e.target.value })}
                />
                <short style={{ color: "red" }}>
                  {this.state.showValidation.includes("password") &&
                    this.validatePassword()}
                </short>
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword3" class="col-sm-2 col-form-label">
                Confirm Password
              </label>
              <div class="col-sm-10">
                <input
                  value={cnfmPassword}
                  type="password"
                  name="cnfmPassword"
                  class="form-control"
                  id="inputPassword3"
                  placeholder="Confirm Password"
                  onBlur={this.showValidation}
                  onInput={this.validateCnfmPassword}
                  onChange={e =>
                    this.setState({ cnfmPassword: e.target.value })
                  }
                />
                <short style={{ color: "red" }}>
                  {this.state.showValidation.includes("cnfmPassword") &&
                    this.validateCnfmPassword()}
                </short>
              </div>
            </div>
            <fieldset class="form-group">
              <div class="row">
                <legend class="col-form-label col-sm-2 pt-0">Gender</legend>
                <div class="col-sm-10">
                  <div class="form-check">
                    <input
                      class="form-check-input"
                      type="radio"
                      name="gridRadios"
                      id="gridRadios1"
                      value="Male"
                      checked={this.state.gender === "Male"}
                      onChange={changeEvent =>
                        this.setState({ gender: changeEvent.target.value })
                      }
                    />
                    <label class="form-check-label" for="gridRadios1">
                      Male
                    </label>
                  </div>
                  <div class="form-check">
                    <input
                      class="form-check-input"
                      type="radio"
                      name="gridRadios"
                      id="gridRadios2"
                      value="Female"
                      checked={this.state.gender === "Female"}
                      onChange={changeEvent =>
                        this.setState({ gender: changeEvent.target.value })
                      }
                    />
                    <label class="form-check-label" for="gridRadios2">
                      Female
                    </label>
                  </div>
                </div>
              </div>
            </fieldset>
            <div class="form-group row">
              <label for="inputPassword3" class="col-sm-2 col-form-label">
                Phone Number
              </label>
              <div class="col-sm-10">
                <input
                  value={mobile}
                  name="mobile"
                  type="number"
                  pattern="[0-9]*"
                  inputmode="numeric"
                  class="form-control"
                  id="inputNumber"
                  placeholder="1234567890"
                  onBlur={this.showValidation}
                  onInput={this.validateNumber}
                  onChange={e => this.setState({ mobile: e.target.value })}
                />
                <short style={{ color: "red" }}>
                  {this.state.showValidation.includes("mobile") &&
                    this.validateNumber()}
                </short>
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword3" class="col-sm-2 col-form-label">
                Types of Member
              </label>
              <div class="col-sm-10">
                <select
                  className="custom-select"
                  value={member}
                  onChange={e => this.setState({ member: e.target.value })}
                >
                  <option>Select menu</option>
                  <option>Entrepreneur</option>
                  <option>Investor</option>
                  <option>Incubator</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-sm-10">
                <button
                  type="submit"
                  class="btn btn-primary"
                  onClick={this.sendData}
                >
                  Submit Form
                </button>
                <p style={{ color: "red" }}>
                  {this.state.fieldRequired ? "*All field are Mandatory" : ""}
                </p>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
export default CoverPage;
