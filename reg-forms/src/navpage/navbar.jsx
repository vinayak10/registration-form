import React, { Component } from "react";
import "./navbar.scss";

class Navbar extends Component {
  render() {
    return (
      <div className="navPage">
        <div className="navLine">
          <nav className="navbar">
            <a className="navbar-brand mb-0 h1" href="#">
              <h4>Registration</h4>
            </a>
          </nav>
        </div>
        <div className="firstLine">
          <div className="social">
            <i className="fa fa-facebook" />
            <i className="fa fa-google-plus" />
            <i className="fa fa-twitter" />
            <i className="fa fa-linkedin" />
          </div>
          <div>
            <div className="contact">
              <div className="mail">
                <i class="fa fa-envelope-o" />
                formregister@example.com
              </div>
              <div className="phone">
                {" "}
                <i class="fa fa-phone" />
                011-111111
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Navbar;
